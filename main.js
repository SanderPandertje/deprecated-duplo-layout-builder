const {app, BrowserWindow, ipcMain} = require('electron')
const path = require('path')
const url = require('url')
const fs = require('fs')

// Global instance of window
let win

function createWindow() {
  // Create the browser window.



  win = new BrowserWindow({
    width: 1600,
    height: 800,
    // frame: false,
    resizable: true,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
    }
  })

  // Load index.html
  win.loadURL(url.format({
    pathname: path.join(__dirname, '/dist/tiled-json-converter/index.html'),
    protocol: 'file:',
    slashes: true
  }))

  //// Uncomment line below to open devtools.
  win.webContents.openDevTools()
}

// Open the app when ready to show
app.whenReady().then(createWindow)

// Macs don't quit apps unless done explicitly so,
// On all operating systems (excluding Mac OS) this code quits the process.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

ipcMain.on('create-dir', function (event, dir) {
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir)
    return true;
  }
  return false;
})

ipcMain.on('save-file', function (event, {path, name, content}) {
    fs.writeFileSync(`${path}/${name}.json`, content, 'utf-8')
})
