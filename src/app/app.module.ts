import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DragDropComponent } from './components/drag-drop/drag-drop.component';
import { UploadFileComponent } from './components/upload-file/upload-file.component';
import { DndDirective } from './directives/dnd.directive';
import { NgxElectronModule} from "ngx-electron";



@NgModule({
  declarations: [
    AppComponent,
    DragDropComponent,
    UploadFileComponent,
    DndDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxElectronModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
