import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {SaveFileService} from '../../services/save-file.service'
import {ElectronService} from "ngx-electron";
import {GameScene} from "../../models/layout/layout-elements/GameScene";
import {BaseElement} from "../../models/layout/layout-elements/BaseElement";


@Component({
  selector: 'app-drag-drop',
  templateUrl: './drag-drop.component.html',
  styleUrls: ['./drag-drop.component.scss']
})
export class DragDropComponent implements OnInit {

  files: File[];
  buttonDisabled: boolean;
  // reader: FileReader;
  saveFileService: SaveFileService;
  private _electronService: ElectronService

  constructor() {
    this._electronService = new ElectronService();
    this.saveFileService = new SaveFileService();
  }

  ngOnInit(): void {
    this.files = [];
    this.buttonDisabled = true;
    // this.reader = new FileReader();
  }

  @ViewChild("fileDropRef", {static: false}) fileDropEl: ElementRef;

  /**
   * on file drop handler
   */
  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  /**
   * handle file from browsing
   */
  fileBrowseHandler($event) {
    this.prepareFilesList($event.target.files);
  }

  /**
   * Delete file from files list
   * @param index (File index)
   */
  deleteFile(index: number) {
    this.files.splice(index, 1);
  }

  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
  prepareFilesList(files: Array<any>) {
    let throwsError = true;
    for (const file of files) {

      // Only accept files called 'game_scene_desktop' and 'game_scene_mobile'. Of which game_scene_mobile
      //   mobile is only accepted when the desktop variant is already present in the array
      if (file.type === "application/json") {
        if (file.name === "game_scene_desktop.json") {
          this.files[0] = file
          throwsError = false;
        }

        if (file.name === "game_scene_mobile.json" && this.files.length > 0) {
          this.files[1] = file
          throwsError = false;
        }

        this.reCalculateButtonStatus()
      } else {


      }
    }

    if (throwsError) {

      // TODO add documentation error article
      // TODO save file with error
      // TODO move error proper function
      alert(`error 001: file(s) rejected! One or more of the uploaded file(s) do not comply with the conventions found in:...\nMore information can be found at: "dir..."`)
    }
    this.fileDropEl.nativeElement.value = "";
  }

  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes, decimals = 2) {
    if (bytes === 0) {
      return "0 Bytes";
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }

  public reCalculateButtonStatus() {
    this.buttonDisabled = this.files.length === 0;
  }

  public commitFiles() {

    for (let file of this.files) {
      let fileReader: FileReader = new FileReader();
      fileReader.readAsText(file)
      fileReader.onload = () => {
        if (typeof fileReader.result === "string") {


          // TODO save file to object
          const gameScene = new GameScene();
          gameScene.startBuildProcess(JSON.parse(fileReader.result))
        }
      }
    }

    // this.saveFileService.save(this.files);
  }
}



