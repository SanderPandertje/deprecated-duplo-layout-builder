export class Mask {
  private _height: number
  private _width: number
  private _x: number
  private _y: number


  constructor(height: number, width: number, x: number, y: number) {
    this._height = height;
    this._width = width;
    this._x = x;
    this._y = y;
  }

  public toString():string {
    return `{"x": ${this._x}, "y": ${this._y}, "width": ${this._width}, "height": ${this._height}}`
  }

  // Getters & Setters
  get height(): number {
    return this._height;
  }

  set height(value: number) {
    this._height = value;
  }

  get width(): number {
    return this._width;
  }

  set width(value: number) {
    this._width = value;
  }

  get x(): number {
    return this._x;
  }

  set x(value: number) {
    this._x = value;
  }

  get y(): number {
    return this._y;
  }

  set y(value: number) {
    this._y = value;
  }
}
