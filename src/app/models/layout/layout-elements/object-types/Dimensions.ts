export class Dimensions {
  private _width: number
  private _height: number

  constructor(height: number, width: number) {
    this._height = height;
    this._width = width;
  }

  public toString():string {
    return `{"width": ${this._width}, "height": ${this._height}}`
  }

  get width(): number {
    return this._width;
  }

  set width(value: number) {
    this._width = value;
  }

  get height(): number {
    return this._height;
  }

  set height(value: number) {
    this._height = value;
  }
}
