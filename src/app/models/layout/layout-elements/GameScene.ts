import {ModelBuilderService} from "../../../services/model-builder.service";
import {BaseElement} from "./BaseElement";
import {Layout} from "../../../interfaces/types/Layout";
import {SaveFile} from "../../misc/SaveFile";
import {SaveFileService} from "../../../services/save-file.service";
import {Container} from "./element-types/Container";

export class GameScene {
  private _elementsMap: Map<number, BaseElement>;
  private mainKey: number;
  private files: SaveFile[];

  constructor(
    private _modelBuilder = new ModelBuilderService,
    private _saveFile = new SaveFileService()) {
    this._elementsMap = new Map();
    this.files = [];
  }

  public startBuildProcess(layout: Layout) {
    console.time('building duration')

    this._elementsMap = this._modelBuilder.startBuild(layout);

    for (const layer of layout.layers) {
      if (layer.name === 'layout') {
        for (let layer1 of layer.layers) {
          if (layer1.hasOwnProperty('objects')) {
            this.mainKey = layer1.objects[0].id
          }
        }
      }
    }
    console.log(this._elementsMap.get(this.mainKey));
    console.log(this._elementsMap);

    console.timeEnd('building duration');

    for (let element of this._elementsMap) {
      if (element[1] instanceof Container) {
        if (element[1].separateFile === true) {
          this.files.push(element[1].toJson());
        }
      }
    }

    this.files.forEach(file => this._saveFile.save(file));
    SaveFileService.resetRootPath();
  }
}
