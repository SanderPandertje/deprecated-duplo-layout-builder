import {Point} from "./object-types/Point";
import {UnresolvedElement} from "../../../interfaces/types/UnresolvedElement";
import {Types} from "../../../interfaces/types/Types";
import {Dimensions} from "./object-types/Dimensions";
import {Mask} from "./object-types/Mask";
import {SaveFile} from "../../misc/SaveFile";
import {Container} from "./element-types/Container";
import {Component} from "./element-types/Component";

export abstract class BaseElement {

  readonly _type: Types;
  readonly _index: number;
  private _id: string;
  private _bindId: string;
  private _position: Point;
  private _pivot: Point;
  private _anchor: Point;
  private _realAnchor: Point;
  private _rPos: Point;
  private _visible: boolean;
  private _global: boolean;
  private _scale: Point;
  private _props: any;

  protected constructor(type: Types, index: number) {
    this._type = type;
    this._index = index
  }

  protected buildBaseElement(unresolvedElement: UnresolvedElement): void {
    const {id, bindId, position, pivot, anchor, realAnchor, rPos, visible, global, scale, props,} = unresolvedElement;
    if (id !== undefined) this.id = id;
    if (bindId !== undefined) this.bindId = bindId;
    if (position !== undefined) this.position = position;
    if (pivot !== undefined) this.pivot = pivot;
    if (anchor !== undefined) this.anchor = anchor;
    if (realAnchor !== undefined) this.realAnchor = realAnchor;
    if (rPos !== undefined) this.rPos = rPos;
    if (visible !== undefined) this.visible = visible;
    if (global !== undefined) this.global = global;
    if (scale !== undefined) this.scale = scale;
    if (props !== undefined) this.props = props;
  }

  public toJson(): SaveFile {
    BaseElement.found = [];
    let properties = new Map<string, UnresolvedElement>();

    for (let [_key, value] of Object.entries(this)) {
      let key = _key.replace('_', '');
      properties.set(key, value);
    }

    let reference = `${properties.get('reference')}`;
    let file: SaveFile;

    let json = `{\n`;
    json += BaseElement.printObject(this);

    if (BaseElement.found.length > 0) {
      for (let container of BaseElement.found) {
        json = `${BaseElement.removeLastChar(json)},\n${BaseElement.printObject(container)}`
      }
    }

    json += `}`;
    json = BaseElement.addTabs(json);

    file = new SaveFile([reference, '@desktop'], `${reference}`, json);
    return file;
  }

  private static found: Container[] = [];

  private static printObject(object: BaseElement): string {
    let json = ``;

    let properties = new Map<string, UnresolvedElement>();

    for (let [_key, value] of Object.entries(object)) {
      let key = _key.replace('_', '');
      properties.set(key, value);
    }

    if (properties.get('useNameAsKey')) {
      json += `"${properties.get('reference')}": `;
    }

    let libReferences;
    if (properties.has('libReferences')) {
      libReferences = properties.get('libReferences');
    }

    properties.delete("found");
    properties.delete('index');
    properties.delete('separateFile');
    properties.delete('reference');
    properties.delete('useNameAsKey');
    properties.delete('libReferences');

    json += `{${this.printElementProperties(properties)}}\n`;

    if (libReferences == undefined) return json;

    for (let libReference of libReferences) {
      if (libReference.useNameAsKey && !libReference.separateFile) {
        this.found.push(libReference)
      }
    }


    return json;
  }

  private static printElementProperties(properties: Map<string, UnresolvedElement>): string {
    let json = '';
    for (let [key, value] of properties) {
      if (key !== 'children') {
        json += BaseElement.printProperty(key, value);
        properties.delete(key);
        if (properties.size !== 0) json += `, `;
      }
    }

    for (let [key, value] of properties) {
      json += `"children": [`;
      if (key === 'children') {
        let children = Object.values(value);
        for (let i = 0; i < children.length; i++) {
          json += `\n`;
          json += this.printObject(children[i]);
          if (i < children.length - 1) {
            json = this.removeLastChar(json);
            json += `,`;
          }
        }
      }
      json += `]`;
    }

    return json
  }

  private static printProperty(key: string, value: UnresolvedElement): string {
    if (
      value instanceof Dimensions ||
      value instanceof Mask ||
      value instanceof Point ||
      key === 'props' ||
      key === 'visible' ||
      key === 'global'
    ) {
      return `"${key}": ${value}`;
    } else {
      return `"${key}": "${value}"`;
    }
  }

  private static removeLastChar(json: string): string {
    return json.substring(0, json.length - 1)
  }

  private static addTabs(unFormattedJson: string): string {
    let json = ``;

    let lines = unFormattedJson.split(/\r?\n/);
    for (let i = 0; i < lines.length; i++) {
      json += `${this.AddTabsToLine(lines[i], json)}\n`;
    }

    return json;
  }

  private static AddTabsToLine(line: string, precedingText: string): string {
    let whitespace = '';
    let tabs =
      (precedingText.match(/[{\[]/mg) || []).length -
      (precedingText.match(/[}\]]/mg) || []).length;

    if ((line.match(/[},\]]/mg) || []).length === line.length) {
      tabs -= (line.match(/[}\]]/mg) || []).length;
    }

    for (let i = 0; i < tabs; i++) {
      whitespace += `\t`;
    }
    return whitespace + line;
  }

  // Getters & Setters

  get id(): string {
    return this._id;
  }

  set id(value: string) {
    this._id = value;
  }

  get bindId(): string {
    return this._bindId;
  }

  set bindId(value: string) {
    this._bindId = value;
  }

  get position(): Point {
    return this._position;
  }

  set position(value: Point) {
    this._position = value;
  }

  get pivot(): Point {
    return this._pivot;
  }

  set pivot(value: Point) {
    this._pivot = value;
  }

  get anchor(): Point {
    return this._anchor;
  }

  set anchor(value: Point) {
    this._anchor = value;
  }

  get realAnchor(): Point {
    return this._realAnchor;
  }

  set realAnchor(value: Point) {
    this._realAnchor = value;
  }

  get rPos(): Point {
    return this._rPos;
  }

  set rPos(value: Point) {
    this._rPos = value;
  }

  get visible(): boolean {
    return this._visible;
  }

  set visible(value: boolean) {
    this._visible = value;
  }

  get global(): boolean {
    return this._global;
  }

  set global(value: boolean) {
    this._global = value;
  }

  get scale(): Point {
    return this._scale;
  }

  set scale(value: Point) {
    this._scale = value;
  }

  get props(): any {
    return this._props;
  }

  set props(value: any) {
    this._props = value;
  }
}
