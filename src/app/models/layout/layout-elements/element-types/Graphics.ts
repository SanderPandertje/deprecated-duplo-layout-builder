import {BaseElement} from "../BaseElement";
import {UnresolvedElement} from "../../../../interfaces/types/UnresolvedElement";
import {Dimensions} from "../object-types/Dimensions";

export class Graphics extends BaseElement {

  private _alpha: number;
  private _color: number;
  private _size: Dimensions;

  constructor(unresolvedElement: UnresolvedElement) {
    const {type, index, alpha, size, color} = unresolvedElement;

    super(type, index);

    if (alpha !== undefined) this.alpha = alpha;
    if (size !== undefined) this.size = size;
    if (color !== undefined) this.color = color;

    super.buildBaseElement(unresolvedElement);
  }

  // Getters & Setters
  get alpha(): number {
    return this._alpha;
  }

  set alpha(value: number) {
    this._alpha = value;
  }

  get color(): any {
    return this._color;
  }

  set color(value: any) {
    this._color = value;
  }

  get size(): any {
    return this._size;
  }

  set size(value: any) {
    this._size = value;
  }
}
