import {BaseElement} from "../BaseElement";
import {UnresolvedElement} from "../../../../interfaces/types/UnresolvedElement";

export class Spine extends BaseElement {

  private _animation: string;
  private _spineId: string;

  constructor(unresolvedElement: UnresolvedElement) {
    const {type, index, spineId} = unresolvedElement;
    super(type, index);

    if (!spineId) throw '__spineId cannnot be undefined';
    this.spineId = spineId;

    super.buildBaseElement(unresolvedElement);
  }

  // Getters & Setters
  get animation(): string {
    return this._animation;
  }

  set animation(value: string) {
    this._animation = value;
  }

  get spineId(): string {
    return this._spineId;
  }

  set spineId(value: string) {
    this._spineId = value;
  }
}
