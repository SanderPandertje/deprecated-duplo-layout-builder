import {BaseElement} from "../BaseElement";
import {Mask} from "../object-types/Mask";
import {UnresolvedElement} from "../../../../interfaces/types/UnresolvedElement";

export class Container extends BaseElement {

  private _reference: string;
  private _mask: Mask;
  private _children: BaseElement[];

  private _separateFile: boolean;
  private _useNameAsKey: boolean

  constructor(unresolvedElement: UnresolvedElement) {
    const {type, index, reference, mask, separateFile, useNameAsKey} = unresolvedElement;
    super(type, index);

    if (!reference) throw 'Native Tiled object name property cannot be undefined.';
    this.reference = reference;
    this._children = [];

    this.separateFile = !!separateFile;
    this.useNameAsKey = !!useNameAsKey;

    if (mask !== undefined) this.mask = mask;

    super.buildBaseElement(unresolvedElement);
  }

  // Getters & Setters
  get reference(): string {
    return this._reference;
  }

  set reference(value: string) {
    this._reference = value;
  }

  get mask(): Mask {
    return this._mask;
  }

  set mask(value: Mask) {
    this._mask = value;
  }

  get children(): BaseElement[] {
    return this._children;
  }

  set children(value: BaseElement[]) {
    this._children = value;
  }

  get separateFile(): boolean {
    return this._separateFile;
  }

  set separateFile(value: boolean) {
    this._separateFile = value;
  }

  get useNameAsKey(): boolean {
    return this._useNameAsKey;
  }

  set useNameAsKey(value: boolean) {
    this._useNameAsKey = value;
  }
}
