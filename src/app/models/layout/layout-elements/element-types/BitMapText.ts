import {BaseElement} from "../BaseElement";
import {UnresolvedElement} from "../../../../interfaces/types/UnresolvedElement";

export class BitMapText extends BaseElement {

  private _text: string;
  private _localeId: string;
  private _memberOf: string;
  private _textType: string;
  private _style: any;


  constructor(unresolvedElement: UnresolvedElement) {
    const {type, index, text, localeId, textType, memberOf, style} = unresolvedElement;
    super(type, index);

    if (!text) throw '__text cannot be undefined!';
    this.text = text;

    if (localeId !== undefined) this.localeId = localeId;
    if (textType !== undefined) this.textType = textType;
    if (memberOf !== undefined) this.memberOf = memberOf;
    if (style !== undefined) this.style = style;

    super.buildBaseElement(unresolvedElement);
  }

  // Getters & Setters
  get text(): string {
    return this._text;
  }

  set text(value: string) {
    this._text = value;
  }

  get localeId(): string {
    return this._localeId;
  }

  set localeId(value: string) {
    this._localeId = value;
  }

  get memberOf(): string {
    return this._memberOf;
  }

  set memberOf(value: string) {
    this._memberOf = value;
  }

  get textType(): string {
    return this._textType;
  }

  set textType(value: string) {
    this._textType = value;
  }

  get style(): any {
    return this._style;
  }

  set style(value: any) {
    this._style = value;
  }
}
