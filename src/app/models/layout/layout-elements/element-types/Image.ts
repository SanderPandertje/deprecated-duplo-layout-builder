import {BaseElement} from "../BaseElement";
import {UnresolvedElement} from "../../../../interfaces/types/UnresolvedElement";

export class Image extends BaseElement {
  private _texture: string;
  private _tint: string;  // TODO convert hex

  constructor(unresolvedElement: UnresolvedElement) {
    const {type, index, texture, tint,} = unresolvedElement;
    super(type, index);

    if (!texture) throw '__texture cannot be undefined';
    this.texture = texture;

    if (tint !== undefined) this.tint = tint;

    super.buildBaseElement(unresolvedElement);
  }

  // Getters & Setters
  get texture(): string {
    return this._texture;
  }

  set texture(value: string) {
    this._texture = value;
  }

  get tint(): string {
    return this._tint;
  }

  set tint(value: string) {
    this._tint = value;
  }
}
