import {BaseElement} from "../BaseElement";
import {Dimensions} from "../object-types/Dimensions";
import {UnresolvedElement} from "../../../../interfaces/types/UnresolvedElement";

export class ResizeArea extends BaseElement{

  private _size: Dimensions;

  constructor(unresolvedElement: UnresolvedElement) {
    const {type, index, size, visible} = unresolvedElement;
    super(type, index);

    if (visible === undefined) throw '__visible cannot be undefined';
    this.visible = visible;

    if (size !== undefined) this._size = size;

    super.buildBaseElement(unresolvedElement);
  }

  // Getters & Setters
  get size(): Dimensions {
    return this._size;
  }

  set size(value: Dimensions) {
    this._size = value;
  }
}
