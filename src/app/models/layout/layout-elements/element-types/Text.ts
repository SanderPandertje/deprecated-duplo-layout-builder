import {BaseElement} from "../BaseElement";
import {UnresolvedElement} from "../../../../interfaces/types/UnresolvedElement";

export class Text extends BaseElement {

  private _localeId: string;
  private _text: string;
  private _style: any;


  constructor(unresolvedElement: UnresolvedElement) {
    const {type, index, localeId, text, style} = unresolvedElement;
    super(type, index);

    if (localeId !== undefined) this.localeId = localeId;
    if (text !== undefined) this.text = text;
    if (style !== undefined) this.style = style

    super.buildBaseElement(unresolvedElement);
  }

  // Getters & Setters
  get style(): any {
    return this._style;
  }

  set style(value: any) {
    this._style = value;
  }

  get localeId(): string {
    return this._localeId;
  }

  set localeId(value: string) {
    this._localeId = value;
  }

  get text(): string {
    return this._text;
  }

  set text(value: string) {
    this._text = value;
  }
}
