import {BaseElement} from "../BaseElement";
import {UnresolvedElement} from "../../../../interfaces/types/UnresolvedElement";
import {Container} from "./Container";
import {Dimensions} from "../object-types/Dimensions";
import {Mask} from "../object-types/Mask";
import {Point} from "../object-types/Point";

export class Component extends BaseElement {


  private _libId: string;
  private _libReferences: Container[];

  constructor(unresolvedElement: UnresolvedElement) {
    const {type, index, libId} = unresolvedElement;

    super(type, index);

    if (!libId) throw '__libId cannot be undefined';
    this.libId = libId;

    this.libReferences = [];

    super.buildBaseElement(unresolvedElement);
  }

// Getters & Setters
  get libReferences(): Container[] {
    return this._libReferences;
  }

  set libReferences(value: Container[]) {
    this._libReferences = value;
  }

  get libId(): string {
    return this._libId;
  }

  set libId(value: string) {
    this._libId = value;
  }
}
