import {Dimensions} from "../object-types/Dimensions";
import {Container} from "./Container";
import {UnresolvedElement} from "../../../../interfaces/types/UnresolvedElement";
import {FitTypes} from "../../../../interfaces/types/FitTypes";

export class GroupContainer extends Container {

  private _base: Dimensions;
  private _fitType: FitTypes;
  private _maxScale: number;

  constructor(unresolvedElement: UnresolvedElement) {
    const {type, index, reference, mask, fitType, rPos, base, maxScale, separateFile, useNameAsKey} = unresolvedElement;
    super({type, index, reference, mask, separateFile, useNameAsKey});

    if (!fitType) throw '__fitType cannot be undefined.';
    if (!rPos) throw '__rPos cannot be undefined.';
    if (!base) throw '__base must be included.';

    this.base = base;
    this.fitType = fitType;
    this.rPos = rPos;

    if (maxScale !== undefined) this.maxScale = maxScale;

    super.buildBaseElement(unresolvedElement);
  }

  // Getters & Setters
  get base(): Dimensions {
    return this._base;
  }

  set base(value: Dimensions) {
    this._base = value;
  }

  get fitType(): FitTypes {
    return this._fitType;
  }

  set fitType(value: FitTypes) {
    this._fitType = value;
  }

  get maxScale(): number {
    return this._maxScale;
  }

  set maxScale(value: number) {
    this._maxScale = value;
  }
}
