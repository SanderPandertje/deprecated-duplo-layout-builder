import {BaseElement} from "../BaseElement";
import {UnresolvedElement} from "../../../../interfaces/types/UnresolvedElement";

export class Button extends BaseElement {

  private _components: any;
  private _states: any;

  constructor(unresolvedElement: UnresolvedElement) {
    const {type, index, states, components} = unresolvedElement;

    super(type, index);

    if (!states) throw '__states cannot be undefined'
    this.states = states;

    if (components !== undefined) this.components = components;

    super.buildBaseElement(unresolvedElement);
  }

  // Getters & Setters
  get components(): any {
    return this._components;
  }

  set components(value: any) {
    this._components = value;
  }

  get states(): any {
    return this._states;
  }

  set states(value: any) {
    this._states = value;
  }
}
