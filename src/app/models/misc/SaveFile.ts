import {SaveFileService} from "../../services/save-file.service";


export class SaveFile {

  private _path: string[];
  private _name: string;
  private _content: string;

  constructor(path: string[], name: string, content: string) {
    this._path = path;
    this._name = name
    this._content = content;
  }

  // Getters & Setters
  get path() {
    return this._path;
  }

  set path(value) {
    this._path = value;
  }

  get name() {
    return this._name;
  }

  set name(value) {
    this._name = value;
  }

  get content() {
    return this._content;
  }

  set content(value) {
    this._content = value;
  }
}
