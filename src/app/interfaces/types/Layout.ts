import {UndefinedElement} from "./UndefinedElement";

export interface Layout {
  height: number;
  width: number;
  layers?: Layer[];
  objects?: UndefinedElement[];
}

export interface Layer {
  name: string;
  layers?: Layer[];
  objects?: UndefinedElement[];
  x: number;
  y: number;
}
