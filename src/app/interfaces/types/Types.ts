export type Types  =
  'Image' |
  'Component' |
  'Container' |
  'Graphics' |
  'GroupContainer' |
  'Spine' |
  'Button' |
  'Text' |
  'BitMapText' |
  'ResizeArea';

