import {BaseElement} from "../../models/layout/layout-elements/BaseElement";
import {Point} from "../../models/layout/layout-elements/object-types/Point";
import {Dimensions} from "../../models/layout/layout-elements/object-types/Dimensions";
import {Mask} from "../../models/layout/layout-elements/object-types/Mask";
import {Types} from "./Types";
import {FitTypes} from "./FitTypes";

export interface UnresolvedElement {
  // Required variables
  type: Types;
  index: number;

  // Optional variables. Depending on the type some properties might be required.
  id?: string;
  bindId?: string;
  position?: Point;
  pivot?: Point;
  anchor?: Point;
  realAnchor?: Point;
  rPos?: Point;
  visible?: boolean;
  global?: boolean;
  scale?: Point;
  props?: any;
  memberOf?: string;
  textType?: string;
  components?: any;
  states?: any;
  libId?: string;
  reference?: string;
  mask?: Mask;
  hitArea?: Mask;
  children?: BaseElement[];
  base?: Dimensions;
  fitType?: FitTypes;
  maxScale?: number;
  texture?: string;
  tint?: string;
  size?: Dimensions;
  animation?: string;
  spineId?: string;
  localeId?: string;
  text?: string;
  style?: any;
  alpha?: number;
  color?: string;

  // Artificial, these variables are used by the algorithm and will therefore not show in the layout.
  zzXAxisOffset?: number;
  dimensions?: Dimensions;
  separateFile?: boolean;
  useNameAsKey?: boolean;
}
