export interface UndefinedElement {
  id: number;
  name: string;
  type: string;
  properties?: Property[]
  x: number;
  y: number;
  width: number;
  height: number;
}

interface Property {
  name: string;
  type: string;
  value: any;
}
