import {Injectable} from '@angular/core';
import {Component} from '../models/layout/layout-elements/element-types/Component';
import {BaseElement} from '../models/layout/layout-elements/BaseElement';
import {BitMapText} from '../models/layout/layout-elements/element-types/BitMapText';
import {Button} from '../models/layout/layout-elements/element-types/Button';
import {Container} from '../models/layout/layout-elements/element-types/Container';
import {GroupContainer} from '../models/layout/layout-elements/element-types/GroupContainer';
import {Image} from '../models/layout/layout-elements/element-types/Image';
import {ResizeArea} from '../models/layout/layout-elements/element-types/ResizeArea';
import {Spine} from '../models/layout/layout-elements/element-types/Spine';
import {Text} from '../models/layout/layout-elements/element-types/Text';
import {UnresolvedElement} from '../interfaces/types/UnresolvedElement';
import {UndefinedElement} from '../interfaces/types/UndefinedElement';
import {Point} from '../models/layout/layout-elements/object-types/Point';
import {Dimensions} from '../models/layout/layout-elements/object-types/Dimensions';
import {Mask} from '../models/layout/layout-elements/object-types/Mask';
import {Layer, Layout} from '../interfaces/types/Layout';
import {Types} from '../interfaces/types/Types';
import {Graphics} from '../models/layout/layout-elements/element-types/Graphics';
import {RegexCheckerService} from './regex-checker.service';

// noinspection TypeScriptFieldCanBeMadeReadonly
@Injectable({
  providedIn: 'root'
})
export class ModelBuilderService {

  private _regexChecker: RegexCheckerService;

  private elementsMap: Map<number, BaseElement>;
  private componentLib: Map<string, Component[]>;
  private window: Dimensions;

  // External element properties. Cannot be saved within method.
  private xOffset: number;

  private static readonly _elementAll = 'SL_ALL';
  private static readonly _elementPrefix = '_SL_';

  // Regex used in the building process


  constructor() {
    this._regexChecker = new RegexCheckerService();

    this.elementsMap = new Map<number, BaseElement>();
    this.componentLib = new Map<string, Component[]>();
    this.window = new Dimensions(0, 0);
    this.xOffset = 0;
  }

  public startBuild(layout: Layout): Map<number, BaseElement> {
    this.window.height = layout.height;
    this.window.width = layout.width;

    console.log(layout);

    if (layout.hasOwnProperty('layers')) {
      for (const layer of layout.layers) {
        if (layer.hasOwnProperty('layers')) {
          if (layer.name === 'layout') {
            this.build(layer.layers);
          }
        }
      }
    } else {
      // TODO proper exception handling
      throw 'Expected structure was not provided, Aborting build process';
    }

    return this.elementsMap;
  }

  private build(layers: Layer[], selectedElement?: Container | GroupContainer): void {
    let builtElement: BaseElement = null;
    let sortedLayers: Layer[] = [];

    try {
      // Reorder the layers array so that the current object is on top.
      // This reorder also places the objects in top (background) to bottom (foreground)

      layers.reverse();

      let origin = layers.length;
      for (let i = layers.length - 1; i > 0; i--) {
        if (!!layers[i].objects) {
          let temp = layers.splice(i, origin - i);
          for (let layer of temp) {
            sortedLayers.push(layer);
          }
          origin = i;
        }
      }

      for (let layer of layers) {
        sortedLayers.push(layer);
      }

      layers = sortedLayers;

      for (const layer of layers) {
        // Build all objects (elements)
        if (layer.hasOwnProperty('objects')) {
          for (const undefinedElement of layer.objects) {

            // Build element
            builtElement = ModelBuilderService.resolveElement(this.defineElement(undefinedElement, selectedElement));
            this.elementsMap.set(builtElement._index, builtElement);

            // Add components to the component map.
            if (builtElement instanceof Component) {
              if (this.componentLib.has(builtElement.libId)) {
                this.componentLib.set(builtElement.libId, [...this.componentLib.get(builtElement.libId), ...[builtElement]]);
              } else {
                this.componentLib.set(builtElement.libId, [builtElement]);
              }
            }
            // Add newly built element to the Children of the parent if present.
            if (selectedElement) {
              selectedElement.children.push(builtElement);
            }
          }
        }

        // Build all layers ((Group)Containers)
        if (layer.hasOwnProperty('layers')) {
          if (builtElement instanceof Container) {
            if (this.componentLib.get(builtElement.reference)) {
              // @ts-ignore
              this.componentLib.get(builtElement.reference).forEach(value => value.libReferences.push(builtElement));
            }
            
            this.build(layer.layers, builtElement);
          } else {
            this.build(layer.layers);
          }
        }
      }

    } catch (e) {
      console.error('Building process was aborted');
      throw e;
    }
  }

  private defineElement(undefinedElement: UndefinedElement, parentElement?: (Container | GroupContainer)): UnresolvedElement {
    let {type, name, properties, id, width, height, x, y} = undefinedElement;

    if (type === ModelBuilderService._elementAll) {
      throw `Element type ${ModelBuilderService._elementAll} is not permitted`;
    }

    // TODO error handling
    type = <Types> type.replace(ModelBuilderService._elementPrefix, '');

    let unresolvedElement: UnresolvedElement = {
      type: <Types> type,
      index: id,
      reference: name,
      dimensions: new Dimensions(height, width)
    };

    if (properties) {
      for (let i = 0; i < properties.length; i++) {
        if (properties[i].name.substr(0, 1) === '_') {
          properties[i].name = properties[i].name.replace(/[_]/g, '');
        } else {
          break;
        }
      }

      for (let i = 0; i < properties.length; i++) {
        const {name, value} = properties[i];

        switch (name) {
          case 'pivot':
          case 'anchor':
          case 'realAnchor':
          case 'rPos':
          case 'scale':

            if (value) {

              if (
                this._regexChecker.xValue(properties[i + 1].name) &&
                this._regexChecker.yValue(properties[i + 2].name)
              ) {

                unresolvedElement[name] = new Point(
                  properties[++i].value,
                  properties[++i].value
                );
                // Replace anchor if realAnchor is used
                if ((unresolvedElement.anchor !== undefined && name === 'realAnchor') ||
                  (unresolvedElement.realAnchor !== undefined && name === 'anchor')) {
                  unresolvedElement.anchor = undefined;
                }

              } else {
                console.error(`Point can not be constructed, expected properties might be missing. Make sure that the following names contain the point's reference name and the X,Y coordinates.`);
                console.log(`%cExample data: ${undefinedElement.name}, ${undefinedElement.name}X, ${undefinedElement.name}Y`, 'font-weight: bold; color: orange;');
                console.log(`Actual data: ${undefinedElement.name}, ${!properties[i + 1] ? 'undefined' : undefinedElement.properties[i + 1].name}, ${!properties[i + 2] ? 'undefined' : undefinedElement.properties[i + 2].name}`);
                throw 'Unexpected datatype';
              }
            }
            break;

          case 'position':
            if (value) {

              unresolvedElement[name] = new Point(
                x,
                y
              );
            }
            break;

          case 'size':
          case 'base':
            if (value) {
              unresolvedElement[name] = new Dimensions(
                width,
                height
              );
            }
            break;

          case 'color':
            console.log('Current color that is being passed: ' + value);
            if (this._regexChecker.hexValue(value)) {
              let color = value;
              console.log(color);

              if (this._regexChecker.hexHas0x(color)) {
                color = color.substring(2);
              }

              console.log(color);

              if (color.length === 3) {
                let hex = color;
                color = '';
                console.log(hex);

                for (let j = 0; j < hex.length; j++) {
                  color += `${hex[j]}${hex[j]}`;
                }

              }

              unresolvedElement[name] = `0x${color}`;

            } else {
              throw `invalid color`;
            }
            break;

          case 'mask':
          case 'hitArea':
            if (value) {
              let correctStructure = true;
              let tempMask: {
                maskHeight?: number,
                maskWidth?: number,
                maskX: number,
                maskY: number,
              } = {maskX: 0, maskY: 0};

              for (let j = 0; j < 2; j++) {
                if (this._regexChecker.dimensions(properties[i + j + 1].name)) {
                  tempMask[properties[i + j + 1].name] = properties[i + j + 1].value;
                } else {
                  correctStructure = false;
                  break;
                }
              }
              for (let j = 0; j < 2; j++) {
                if (this._regexChecker.coordinates(properties[i + j + 3].name)) {
                  tempMask[properties[i + j + 3].name] = properties[i + j + 3].value;
                } else {
                  console.log(`${name} with id: ${id}, does not contain all the coordinates, empty values have been replaced with: 0`);
                }
              }
              if (correctStructure) {
                unresolvedElement[name] = new Mask(
                  tempMask.maskHeight,
                  tempMask.maskWidth,
                  tempMask.maskX,
                  tempMask.maskY
                );
              } else {
                console.error(`${name} with id: ${id}, cannot be constructed.`);
                throw 'Unexpected datatype';
              }
            }
            break;

          default:
            // Ignore any Constructable object, to prevent errors.
            if (!(['positionX', 'positionY', 'pivotX', 'pivotY', 'anchorX', 'anchorY', 'realAnchorX', 'realAnchorY', 'rPosX',
              'rPosY', 'maskX', 'maskY', 'maskHeight', 'maskWidth',]
              .indexOf(name) >= 0)) {
              unresolvedElement[name] = value;
            }
            break;
        }
      }

      if (unresolvedElement.type === 'ResizeArea' && (parentElement.anchor || parentElement.realAnchor)) {
        if (parentElement.anchor) {
          this.xOffset = ((this.window.width - width) * (1 - parentElement.anchor.x));
        }
        if (parentElement.realAnchor) {
          this.xOffset = ((this.window.width - width) * (1 - parentElement.realAnchor.x));
        }
      }

      if (!!unresolvedElement.position) {
        if (unresolvedElement.anchor || unresolvedElement.realAnchor || unresolvedElement.scale) {
          let anchor;

          if (unresolvedElement.anchor !== undefined || unresolvedElement.realAnchor !== undefined) {
            if (!!unresolvedElement.anchor) {
              anchor = unresolvedElement.anchor;
            }
            if (!!unresolvedElement.realAnchor) {
              anchor = unresolvedElement.realAnchor;
            }
          } else {
            throw 'if a scale is given, anchor is mandatory';
          }

          unresolvedElement.position.x += (unresolvedElement.dimensions.width * anchor.x);
          unresolvedElement.position.y -= unresolvedElement.dimensions.height * (1 - anchor.y);
        }
        unresolvedElement.position.x -= this.xOffset;
      }
    }

    return unresolvedElement;
  }

  private static resolveElement(unresolvedElement: UnresolvedElement): BaseElement {
    try {
      switch (unresolvedElement.type) {
        case 'BitMapText':
          return new BitMapText(unresolvedElement);

        case 'Button':
          return new Button(unresolvedElement);

        case 'Component':
          return new Component(unresolvedElement);

        case 'Container':
          return new Container(unresolvedElement);

        case 'Graphics':
          return new Graphics(unresolvedElement);

        case 'GroupContainer':
          return new GroupContainer(unresolvedElement);

        case 'Image':
          return new Image(unresolvedElement);

        case 'ResizeArea':
          return new ResizeArea(unresolvedElement);

        case 'Spine':
          return new Spine(unresolvedElement);

        case 'Text':
          return new Text(unresolvedElement);
      }
    } catch (e) {
      // TODO proper exception handling
      console.error(`Expected structure was not provided, element could not be resolved.`);
      throw e;
    }
    console.error(`Element type ${unresolvedElement.type} does not exist`);
    throw 'reference error';
  }
}
