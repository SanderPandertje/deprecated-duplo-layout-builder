import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RegexCheckerService {

  private readonly regex = {
    dimensions: RegExp(/^(mask)(Height|Width)$/),
    coordinates: RegExp(/^(mask)([XY])$/),
    xValue: RegExp(/[X]$/),
    yValue: RegExp(/[Y]$/),
    hexValue: RegExp(/^(0x|)([0-9A-F]{3}|[0-9A-F]{6})$/gmi),
    hexHas0x: RegExp(/^[0x]/gmi),
  };

  constructor() {
  }

  public dimensions(dimensions: string): boolean {
    return this.regex.dimensions.test(dimensions);
  }

  public coordinates(coordinates: string): boolean {
    return this.regex.coordinates.test(coordinates);
  }

  public xValue(xValue: string): boolean {
    return this.regex.xValue.test(xValue);
  }

  public yValue(yValue: string): boolean {
    return this.regex.yValue.test(yValue);
  }


  public hexValue(hexValue: string): boolean {
    return this.regex.hexValue.test(hexValue);
  }

  public hexHas0x(hexHas0x: string): boolean {
    return this.regex.hexHas0x.test(hexHas0x);
  }


}
