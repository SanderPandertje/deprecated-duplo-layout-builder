import {Injectable} from '@angular/core';
import {ElectronService} from "ngx-electron";
import {OpenDialogSyncOptions} from "electron";
import {SaveFile} from "../models/misc/SaveFile";


@Injectable({
  providedIn: 'root'
})
export class SaveFileService {

  private _electronService: ElectronService;
  private static rootPathIsSet = false;
  private static rootPath = '';

  constructor() {
    this._electronService = new ElectronService();
  }

  public save(file: SaveFile) {

    const options: OpenDialogSyncOptions = {
      title: 'Select location to save layout',
      defaultPath: file.path[0],
      properties: [
        "createDirectory",
        "openDirectory"
      ],
    }

    if (!SaveFileService.rootPathIsSet) {
      SaveFileService.rootPath = `${this._electronService.remote.dialog.showOpenDialogSync(options)[0]}/layouts`;
      if (SaveFileService.rootPath === '/layouts') {
        console.log('cancelled')
        return;
      }

      this._electronService.ipcRenderer.send('create-dir', SaveFileService.rootPath);
      SaveFileService.rootPathIsSet = true;
    }

    let path = SaveFileService.rootPath;

    for (let subDirectory of file.path) {
      path += `/${subDirectory}`;
      this._electronService.ipcRenderer.send('create-dir', path);
    }

    this._electronService.ipcRenderer.send('save-file', {path: path, name: file.name, content: file.content});
  }

  public static resetRootPath() {
    this.rootPathIsSet = false;
    this.rootPath = '';
  }
}
